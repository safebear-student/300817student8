package com.safebear.app;

import com.safebear.app.utils.Data;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {

    @Test
    public void testLogin() throws Exception {
        // Step 1 chek we are on Welcome page
        assertTrue(welcomePage.checkCorrectPage());

        // Step 2 click on login page
        assertTrue(welcomePage.clickOnLogin(loginPage));

        // Step 2 click on login page
        assertTrue(loginPage.login(userPage, Data.USERNAME, Data.PASSWORD));

    }

}
