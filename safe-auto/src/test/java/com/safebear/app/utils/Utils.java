package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;

public class Utils {

    private WebDriver driver;
    private String url;

    public boolean navigateToWebsite(WebDriver driver) {
        this.driver = driver;
        url = "http://automate.safebear.co.uk";
        driver.get(url);

        return driver.getTitle().startsWith("Welcome");
    }

}
