package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class BaseTest {

    private WebDriver driver;
    protected WelcomePage welcomePage;
    protected LoginPage loginPage;
    protected UserPage userPage;

    @Before
    public void setUp() {
        Utils utility = new Utils();
        //driver = new ChromeDriver();

        DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
        try {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub      zaZZza      AZZazazazazazazazazazazazazazazazazazazazazazazazazazaz"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);

        assertTrue(utility.navigateToWebsite(driver));
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
