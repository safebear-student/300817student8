package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage {

    private WebDriver driver;

    public WelcomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(linkText = "Login")
    WebElement loginButton1;

    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[2]/a")
    WebElement loginButton2;


    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Welcome");
    }

    public boolean clickOnLogin(LoginPage loginPage){
        loginButton1.click();
        return loginPage.checkCorrectPage();
    }

}
