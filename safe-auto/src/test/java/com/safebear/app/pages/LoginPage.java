package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "myid")
    WebElement loginInput;

    @FindBy(id = "mypass")
    WebElement passwordInput;

    @FindBy(xpath = "/html/body/div/div/div/form/button")
    WebElement submitButton;

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Sign In");
    }

    public boolean login (UserPage userPage, String id, String password){
        loginInput.sendKeys(id);
        passwordInput.sendKeys(password);
        submitButton.submit();
        return userPage.checkCorrectPage();
    }

}
